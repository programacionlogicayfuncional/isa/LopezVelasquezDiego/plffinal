(ns plffinal.core)

(defn regresa
  []
  (hash-map
   \< 1
   \> -1
   \^ 1
   \v -1))

(defn regresa_2
  []
  (hash-map
   \< \>
   \> \<
   \^ \v
   \v \^))

;; Problema 1

(defn intercambiar
  [xs]
  (let [tabla (regresa)
        compara (fn [z] (let [x (tabla z)] (if (int? x) x z)))]
    (flatten (map (fn [w] (map compara w)) (vector xs)))))

(defn regresa-al-punto-de-origen?
  [s]
  (if (zero? (reduce + (intercambiar s)))
    true
    false))

(regresa-al-punto-de-origen? (list \> \<))

;; problema 2

(defn remplazar
  [xs]
  (let [tabla (regresa)
        compara (fn [z] (let [x (tabla z)] (if (int? x) x z)))]
    (map (fn [w] (map compara w)) xs)))

(defn regresan-al-punto-de-origen?
  [x]
  (if (zero? (reduce + (flatten (remplazar x))))
    true
    false))

(regresan-al-punto-de-origen? (vector ">>>" "^vv^" "<<>>"))

;;problema 3

(defn regreso-al-punto-de-origen
  [xs]
  (let [tabla (regresa_2)
        compara (fn [z] (let [x (tabla z)] (if (char? x) x z)))]
    (flatten (map (fn [w] (map compara w)) (vector xs)))))

(regreso-al-punto-de-origen [\< \v \v \v \> \>])

;;problema 4

(defn mismo-punto-final?
  [x y]
  (if (= (reduce + (flatten (remplazar (vector x)))) (reduce + (flatten (remplazar (vector y)))))
    true
    false))

(mismo-punto-final? [\> \> \>] [\> \> \> \>])
